from enum import Enum
class RemoteEnum(Enum):
    ATOL = "atol"
    TOPPING = "topping"
    TV = "tv"