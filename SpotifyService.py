import spotipy
from spotipy.oauth2 import SpotifyOAuth
import logging
from random import randrange

class SpotifyService:
    def __init__(self):
        self.logger = logging.getLogger("SpotifyService")
        scope = "user-read-playback-state,user-modify-playback-state,playlist-read-private"
        self.sp = spotipy.Spotify(auth_manager=SpotifyOAuth(scope=scope, client_id='929ced46a6c841a285cec57c323d88c1',
                                                     client_secret='b41db4369271472591ed8b3f2255f9e9',
                                                     redirect_uri="http://oreo-s-crib.go.ro/"))


    def playPlaylist(self, playlistName):
        self.logger.info("playPlaylist: Going to play " + playlistName)
        device = self.getRpiDevice()
        if device == None:
            self.logger.error("Spotify device was not found")
            return

        playlist = self.searchPlaylist(playlistName)
        self.play(playlist, device['id'])


    def playArtist(self, artistName):
        self.logger.info("playArist: Going to play songs by " + artistName)

    def playNextTrack(self):
        self.logger.info("playNextTrack")
        device = self.getRpiDevice()
        if device == None:
            self.logger.error("Spotify device was not found")
            return

        self.logger.info("Going to play next track")
        self.sp.next_track(device_id=device['id'])


    def playPrevTrack(self):

        self.logger.info("playPrevTrack")
        device = self.getRpiDevice()
        if device == None:
            self.logger.error("Spotify device was not found")
            return

        self.logger.info("Going to play prev track")
        self.sp.previous_track(device_id=device['id'])

    def pauseMusic(self):

        self.logger.info("pauseMusic")
        device = self.getRpiDevice()
        if device == None:
            self.logger.error("Spotify device was not found")
            return

        self.logger.info("Going to pause music")
        self.sp.pause_playback(device_id=device['id'])

    def startMusic(self):

        self.logger.info("startMusic/Unpause")
        device = self.getRpiDevice()
        if device == None:
            self.logger.error("Spotify device was not found")
            return

        self.logger.info("Going to start/unpause music")
        self.sp.start_playback(device_id=device['id'])

    def getRpiDevice(self):
        # Shows playing devices
        rpi = None
        device = self.sp.devices()

        for i in device['devices']:
            if i['name'] == 'Speakers':
                rpi = i
                self.logger.info("rpi device was found: " + rpi['name'])
                break

        if rpi == None:
            self.logger.info ("RPI device was not found")

        return rpi


    def searchPlaylist(self, name):

        playlist = None
        results = self.sp.current_user_playlists(limit=50)

        for i, item in enumerate(results['items']):
            if name.lower() == item['name'].lower():
                #self.logger.info ("Found playlist: " + name)
                playlist = item
                break
            self.logger.info(item['name'])

        if playlist == None:
            self.logger.info("Error: Playlist " +name+ " was not found !")
        return playlist




    def play(self, playlist, deviceId):
        self.sp.start_playback(context_uri=playlist['uri'], device_id=deviceId, offset={"position": randrange(playlist['tracks']['total'] - 1)})
        self.sp.shuffle(device_id=deviceId, state=True)
        self.sp.volume(100)
        self.logger.info("done")



