import os
import time
import logging

from remote import RemoteEnum
class IRService:
  def __init__(self):
    print("IRService initialized")

  def performEvent(self, remote, action, value):
    self.logger = logging.getLogger("SpotifyService")
    print("going to trigger an event: " + remote + " " + action)

    if remote.upper() == RemoteEnum.ATOL.name:
      self.__performAtolEvent(action, value)
    elif remote.upper() == RemoteEnum.TOPPING.name:
      self.__performToppingEvent(action)
    elif remote.upper() == RemoteEnum.TV.name:
      self.__performTvEvent(action)
    else:
      self.logger.info("Invalid remote name: " + remote)


  def __performAtolEvent(self, action, value):
    self.logger.info("__performAtolEvent" + action)
    #custom excetion, TODO: handle this better?
    if action == "ATOL_VOLUME_UP_3":
      for number in range(3):
        self.__perform(self.__generateAction(RemoteEnum.ATOL.name, "ATOL_VOLUME_UP"))
        time.sleep(0.3)
    elif action == "ATOL_VOLUME_DOWN_3":
      for number in range(3):
        self.__perform(self.__generateAction(RemoteEnum.ATOL.name, "ATOL_VOLUME_DOWN"))
        time.sleep(0.3)
    elif action == "ATOL_SET_VOLUME":

      self.logger.info("set volume to " + value)
      for number in range(40):
        self.__perform(self.__generateAction(RemoteEnum.ATOL.name, "ATOL_VOLUME_DOWN"))
        time.sleep(0.1)
      value = int(value)
      maxVolume = 50
      if value >= maxVolume:
          value = maxVolume
      for number in range(value):
        self.__perform(self.__generateAction(RemoteEnum.ATOL.name, "ATOL_VOLUME_UP"))
        time.sleep(0.1)

    #regular command
    else:
      self.__perform(self.__generateAction(RemoteEnum.ATOL.name, action))


  def __performToppingEvent(self, action):
    self.logger.info("__performToppingEvent" + action)
    self.__perform(self.__generateAction(RemoteEnum.TOPPING.name, action))

  def __performTvEvent(self, action):
    self.logger.info("__performTvEvent" + action)
    self.__perform(self.__generateAction(RemoteEnum.TV.name, action))


  def __generateAction(self, remote, action):
    command = "irsend SEND_ONCE "+ remote.lower() + " " + action
    return command

  def __perform(self, action):
    os.system(action)
